import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './tabbar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen(
    this.controller, {
    Key? key,
  }) : super(key: key);

  final ScrollController controller;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final String _tweetText =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  final _randomImage = "https://picsum.photos/200/300";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //  floatingActionButton: _fabButton,
        body: _listView);
  }

  Widget get _listView => ListView.builder(
        itemCount: 10,
        // controller: widget.controller,

        itemBuilder: (context, index) {
          return _listViewCard;
        },
      );

  Widget get _listViewCard => Card(
        child: ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(_randomImage),
          ),
          title: Wrap(
            runSpacing: 10,
            children: [
              _listCardTitle("Hello"),
              Text(_tweetText),
              _placeHolderField,
              _footerButtonRow
            ],
          ),
        ),
      );

  Widget _listCardTitle(String text) => Text(text, style: titleTextStyle);

  Widget get _placeHolderField => Container(
        height: 100,
        child: const Placeholder(),
      );

  Widget get _footerButtonRow => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _iconLabelButton,
          _iconLabelButton,
          _iconLabelButton,
          _iconLabelButton,
        ],
      );

  Widget _iconLabel(String text) => Wrap(
        spacing: 5,
        children: [
          const Icon(
            Icons.comment,
            color: CupertinoColors.inactiveGray,
            size: 25,
          ),
          Text(text)
        ],
      );

  Widget get _iconLabelButton => InkWell(
        child: _iconLabel("1"),
        onTap: () {},
      );
}
