import 'package:flutter/material.dart';
import '../model/topics.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final double _padding = 20;
  late Topics _topics;

  @override
  void initState() {
    super.initState();
    _topics = Topics(
        hashtag: "#Trending in Vietnam",
        location: "hihi",
        tweet: "38.5K Tweets");
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        _downIconWidget,
        _emptySpace,
        _trendTitleWidget,
        _listHashView
      ],
    );
  }

  Widget get _emptySpace => const SizedBox(height: 10);

  Widget get _downIconWidget => Container(
        child: const Icon(
          Icons.arrow_downward,
          color: Colors.grey,
        ),
      );

  Widget get _trendTitleWidget => Card(
        color: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
            side: const BorderSide(color: Colors.grey, width: 0.5)),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: _padding),
          alignment: Alignment.centerLeft,
          height: 50,
          child: Text("Trends For You",
              style: Theme.of(context).textTheme.headline5),
        ),
      );

  Widget get _divider => const Divider(height: 0, color: Colors.grey);

  Widget get _listHashView => ListView.separated(
        itemCount: 5,
        physics: const NeverScrollableScrollPhysics(),
        separatorBuilder: (context, index) => _divider,
        shrinkWrap: true,
        itemBuilder: (context, index) => Card(
          margin: const EdgeInsets.only(bottom: 10),
          shape: const RoundedRectangleBorder(
              side: BorderSide(style: BorderStyle.none)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: _padding, vertical: 5),
            child: NewWidget(topics: _topics),
          ),
        ),
      );
}

class NewWidget extends StatelessWidget {
  const NewWidget({
    Key? key,
    required Topics topics,
  })  : _topics = topics,
        super(key: key);

  final Topics _topics;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            child: Wrap(
          direction: Axis.vertical,
          spacing: 5,
          children: [
            Text(_topics.location, style: Theme.of(context).textTheme.caption),
            Text(_topics.hashtag,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    ?.copyWith(fontSize: 15)),
            Text(_topics.tweet, style: Theme.of(context).textTheme.button)
          ],
        )),
        const Icon(Icons.arrow_drop_down)
      ],
    );
  }
}
